package no.acclerate.hibernatetestgradle.services;

import no.acclerate.hibernatetestgradle.models.Professor;

/**
 * Service for the Professor domain class.
 * Providing basic CRUD functionality through CrudService and any extended functionality.
 */
public interface ProfessorService extends CrudService<Professor, Integer> {
}
