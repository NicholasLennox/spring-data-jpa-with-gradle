# Spring Data JPA with Gradle

[![standard-readme compliant](https://img.shields.io/badge/standard--readme-OK-green.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)

This project serves as a resource for learning Hibernate with Spring Data JPA. 
There are some aspects left out or not done completely correct as this requires a more complete context with Spring Web at a later stage.

## Table of Contents

- [Install](#install)
- [Usage](#usage)
- [Maintainers](#maintainers)
- [Contributing](#contributing)
- [License](#license)

## Install

Bash:

```bash
git clone 
cd HibernateTestGradle
./gradlew wrapper
./gradlew build
```

Powershell:

```bash
git clone 
cd HibernateTestGradle
.\gradlew wrapper
.\gradlew build
```

## Usage

Bash:

```bash
./gradlew bootRun
```

Powershell:

```bash
.\gradlew bootRun
```

## Maintainers

[@NicholasLennox](https://gitlab.com/NicholasLennox)

## Contributing

PRs accepted.

Small note: If editing the README, please conform to the [standard-readme](https://github.com/RichardLitt/standard-readme) specification.

## License

MIT © 2022 Nicholas Lennox
